import { Router, NavigationExtras } from '@angular/router';
import { EventsService } from 'src/app/service/events.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
list:any;
fileToUpload: File | null = null;
  // router: any;
  constructor(private service:EventsService  ,private router:Router) {
    
   }

  ngOnInit(): void {
    this.getall();
    this.deleteEvents;


  }

  getall():void{

this.service.getAll().subscribe({next: (data) => {

this.list= data;

console.log(data);

},

error: (e) => console.error(e)

});

  }
  deleteEvents(id:number){
    this.service. supprimer(id)
      .subscribe(data => {
        this.deleteEvents=data;
        console.log(data);
      },
        error => console.log(error));

  }
  update(eve:any){
    this.router.navigate(['admin/editevents' ,eve]);
    let navigationExtras:NavigationExtras={

      queryParams:{

      special:JSON.stringify(eve)

      }

      }

      this.router.navigate(['admin/editevents'],navigationExtras);

  }




}
