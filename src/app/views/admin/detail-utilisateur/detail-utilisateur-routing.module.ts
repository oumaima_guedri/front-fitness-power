import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailUtilisateurComponent } from './detail-utilisateur.component';

const routes: Routes = [
  {path:'',component:DetailUtilisateurComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailUtilisateurRoutingModule { }
